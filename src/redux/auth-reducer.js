import {authAPI} from "../api/api";

const SET_USER_DATA = 'SET_USER_DATA';

let initialState = {
  login: '',
  isAuth: false
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_DATA:
      return {
        ...state,
        login: action.login,
        isAuth: true
      };
    default:
      return state;
  }
};

export const setLogin = (login, password) => async (dispatch) => {
  let response = await authAPI.login(login, password);
  dispatch(setAuthUserData(response.data));
};

export const setLogout = () => async (dispatch) => {
  let response = await authAPI.logout();
  dispatch(setAuthUserData(response.data));
};

export const checkMe = () => async (dispatch) => {
  let response = await authAPI.me();
  dispatch(setAuthUserData(response.data));
};

export const setAuthUserData = ({login}) => (
  {
    type: SET_USER_DATA,
    login
  });

export default authReducer;