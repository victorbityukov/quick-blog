import {articlesAPI} from "../api/api";

const SET_ARTICLES = 'SET_ARTICLES';
const SET_CURRENT_ARTICLE = 'SET_CURRENT_ARTICLE';
const UPDATE_ARTICLE = 'UPDATE_ARTICLE';

let initialState = {
  isReadyList: false,
  items: [],
  nextId: 0,
  isReadyCurrent: false,
  current: {},
};

const articlesReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ARTICLES:
      return {
        ...state,
        items: action.data.items,
        nextId: action.data.nextId,
        isReadyList: true
      };
    case SET_CURRENT_ARTICLE:
      return {
        ...state,
        isReadyCurrent: true,
        current: action.article,
      };
    case UPDATE_ARTICLE:
      return {
        ...state,
        isReadyCurrent: false,
        isReadyList: false
      };
    default:
      return state;
  }
};

export const setArticles = (data) => (
  {
    type: SET_ARTICLES,
    data: data
  });

export const setCurrentArticle = (article) => (
  {
    type: SET_CURRENT_ARTICLE,
    article
  });

export const updateArticle = () => (
  {
    type: UPDATE_ARTICLE
  });

export const getCurrentArticleId = (id) => async (dispatch) => {
  let responseArticle = await articlesAPI.getCurrentArticleId(id);
  dispatch(setCurrentArticle(responseArticle.data));
};

export const getCurrentArticle = (hash) => async (dispatch) => {
  let responseArticle = await articlesAPI.getCurrentArticle(hash);
  dispatch(setCurrentArticle(responseArticle.data));
};

export const getListArticles = () => async (dispatch) => {
  let responseArticles = await articlesAPI.getListArticles();
  dispatch(setArticles(responseArticles.data));
};

export const setUpdateArticle = (data) => async (dispatch) => {
  await articlesAPI.updateArticle(data);
  dispatch(updateArticle());
};

export const deleteArticle = (id) => async (dispatch) => {
  await articlesAPI.deleteArticle(id);
  dispatch(updateArticle());
};

export default articlesReducer;