import {applyMiddleware, combineReducers, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import authReducer from "./auth-reducer";
import articlesReducer from "./articles-reducer";

let reducers = combineReducers({
  auth: authReducer,
  articles: articlesReducer,
});

// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// const store = createStore(reducers,  composeEnhancers(applyMiddleware(thunkMiddleware)));
const store = createStore(reducers,  applyMiddleware(thunkMiddleware));
window.__store__ = store;

export default store;