import React, {Component} from 'react';
import './App.css';
import {connect, Provider} from "react-redux";
import {BrowserRouter} from "react-router-dom";
import {checkMe} from "./redux/auth-reducer";
import Contents from "./components/Contents/Contents";

class App extends Component {
  componentDidMount() {
    this.props.checkMe();
  }

  render() {
    return (
      <div className="App">
        <Provider store={this.props.store}>
          <BrowserRouter>
            <Contents/>
          </BrowserRouter>
        </Provider>
      </div>
    );
  }
}


let mapStateToProps = () => {
  return {}
};

let mapDispatchToProps = (dispatch) => {
  return {
    checkMe: () => {
      dispatch(checkMe());
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
