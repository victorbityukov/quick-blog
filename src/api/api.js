import * as axios from "axios";
import {contains} from "../utils/functions";

const instance = axios.create({
  baseURL: 'https://max.readman.pro/api/',
  // baseURL: 'http://localhost/maxquickblog/api/',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  },
  withCredentials: true
});

export const authAPI = {
  me() {
    let formData = new FormData();
    formData.append('event', 'check');
    return instance.post(`auth`, formData).then((response) => {
      return response.data;
    });
  },
  login(login, password) {
    let formData = new FormData();
    formData.append('event', 'login');
    formData.append('login', login);
    formData.append('password', password);
    return instance.post(`auth`, formData).then((response) => {
      return response.data;
    });
  },
  logout() {
    let formData = new FormData();
    formData.append('event', 'logout');
    return instance.post(`auth`, formData).then((response) => {
      return response.data;
    });
  }
};

//статьи
export const articlesAPI = {
  getListArticles() {
    let formData = new FormData();
    formData.append('event', 'getListArticles');
    return instance.post(`articles`, formData).then((response) => {
      return response.data;
    });
  },
  getCurrentArticleId(id) {
    let formData = new FormData();
    formData.append('id', id);
    formData.append('event', 'getCurrentArticleId');
    return instance.post(`articles`, formData).then((response) => {
      return response.data;
    });
  },

  getCurrentArticle(hash) {
    let formData = new FormData();
    formData.append('hash', hash);
    formData.append('event', 'getCurrentArticleHash');
    return instance.post(`articles`, formData).then((response) => {
      return response.data;
    });
  },
  updateArticle(data) {
    let fields = ['id', 'title', 'author', 'content', 'date_published'];
    let formData = new FormData();
    Object.keys(data).forEach((key) => {
      if (contains(fields, key)) {
        formData.append(key, data[key]);
      }
    });
    formData.append('event', 'newArticle');
    return instance.post(`articles`, formData).then((response) => {
      return response.data;
    });
  },
  deleteArticle(id) {
    let formData = new FormData();
    formData.append('event', 'deleteArticle');
    formData.append('id', id);
    return instance.post(`articles`, formData).then((response) => {
      return response.data;
    });
  },
};



