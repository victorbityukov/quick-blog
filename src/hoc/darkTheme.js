import React from "react";
import {connect} from "react-redux";
import {changeTheme} from "../redux/header-reducer";

let mapStateToProps = ({header}) => {
  return {
    theme: header.theme
  }
};

let mapDispatchToProps = (dispatch) => {
  return {
    changeTheme: (theme) => dispatch(changeTheme(theme))
  }
};

export const darkTheme = (Component) => {

  class darkThemeComponent extends React.Component {
    componentDidMount() {
      this.props.changeTheme('dark');
    }

    componentWillUnmount() {
      this.props.changeTheme('light');
    }

    render() {
      return <Component {...this.props}/>
    }
  }

  return connect(mapStateToProps, mapDispatchToProps)(darkThemeComponent);

};


