import React from 'react';
import {Link, useHistory} from "react-router-dom";
import {getListArticles} from "../../../redux/articles-reducer";
import {connect} from "react-redux";
import style from './Articles.module.css';

const Articles = ({isReady, items, getList}) => {
  let history = useHistory();

  if (!isReady) {
    getList();
  }
  const onLogout = () => {
    history.push('/auth/logout');
  };

  return (
    <div>
      <button onClick={onLogout}>Выйти</button>
      <h2>Статьи</h2>
      <Link className={`${style['btn']} ${style['btn-add']}`} to={`/auth/article/edit/new`}>Новая статья</Link>
      {items.length > 0 ?
        <>
          {items.map((item) => (
            <div className={style['line']} key={item.id}>
              <span><Link className={style['btn']} to={`/auth/article/delete/${item.id}`}>Удалить</Link></span>
              <span><Link className={style['btn']} to={`/auth/article/edit/${item.id}`}>Изменить</Link></span>
              <span><Link className={style['title']} to={`/article/${item.link}`}>{item.title}</Link></span>
            </div>
          ))}
        </> : "Список статей пуст"
      }
    </div>);
};

let mapStateToProps = ({articles}) => {
  return {
    isReady: articles.isReadyList,
    items: articles.items
  }
};

let mapDispatchToProps = dispatch => {
  return {
    getList: () => dispatch(getListArticles())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Articles);