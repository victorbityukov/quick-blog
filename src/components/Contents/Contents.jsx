import React from 'react';
import {Route, Switch} from "react-router-dom";
import Auth from "../Auth/Auth/Auth";
import RedirectForAuth from "../common/RedirectForAuth";
import ArticleView from "./Article/ArticleView/ArticleView";
import style from "./Contents.module.css";


const Contents = () => {
  return (
    <div className={style['content']}>
      <Switch>
        <Route path="/article/:hash" component={ArticleView}/>
        <Route path="/auth" component={Auth}/>
        <Route path="/" component={RedirectForAuth}/>
      </Switch>
    </div>
  );
};

export default Contents;