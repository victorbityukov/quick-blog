import React from 'react';
import {connect} from "react-redux";
import {getCurrentArticle} from "../../../../redux/articles-reducer";
import Content from "../../../common/Content/Content";
import style from './ArticleView.module.css';


const ArticleView = ({match, current, getCurrentArticle}) => {
  let hash = match.params.hash;
  let isReady = true;
  if (current.link !== hash) {
    isReady = false;
    getCurrentArticle(hash);
  }
  return (
    <>
      {(isReady) ? <ArticleContent {...current}/> : ''}
    </>
  )
};

let mapStateToProps = ({articles}) => {
  return {
    isReadyArticle: articles.isReadyCurrent,
    current: articles.current
  }
};

let mapDispatchToProps = (dispatch) => {
  return {
    getCurrentArticle: (hash) => {
      dispatch(getCurrentArticle(hash));
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ArticleView);

function ArticleContent(current) {
  return (
    <div>
      {!!current.empty ? 'Статья не найдена' : <>
        <h1>{current.title}</h1>
        <h4 className={style['meta']}>{current.author} &#8729; {current.date_published}</h4>
        <Content>
          {current.content}
        </Content>
      </>}
    </div>
  );
}

