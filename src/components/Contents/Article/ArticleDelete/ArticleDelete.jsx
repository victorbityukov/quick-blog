import React from 'react';
import {Redirect, useHistory} from "react-router-dom";
import {connect} from "react-redux";
import {deleteArticle} from "../../../../redux/articles-reducer";

const ArticlesDelete = ({match, isReady, items, deleteArticle}) => {
  let history = useHistory();
  let id = Number(match.params.id);
  if (!id || !isReady) {
    return <Redirect to={"/auth/articles"}/>
  }

  const moveOnList = () => {
    history.push('/auth/articles');
  };

  const deleteItem = () => {
    deleteArticle(id);
  };
  let title = '';
  items.forEach((item) => {
    if (item.id === id) {
      title = item.title;
    }
  });
  return <>
    <h1>Действительно хотите удалить статью?</h1>
    <h2>"{title}"</h2>
    <button onClick={deleteItem}>Да</button>
    <button onClick={moveOnList}>Отмена</button>
  </>
};

let mapStateToProps = ({articles}) => {
  return {
    isReady: articles.isReadyList,
    items: articles.items
  }
};

let mapDispatchToProps = dispatch => ({
  deleteArticle: id => dispatch(deleteArticle(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(ArticlesDelete);