import React, {Component} from 'react'
import ArticleEditForm from "./ArticleEditForm/ArticleEditForm";
import {getCurrentArticleId, updateArticle} from "../../../../redux/articles-reducer";
import {connect} from "react-redux";

class ArticleEdit extends Component {
  constructor(props) {
    super(props);
    this.moveOnList = this.moveOnList.bind(this);
  }

  componentDidMount() {
    let id = this.props.match.params.id;
    if (!this.props.isReadyList) {
      this.moveOnList();
    }
    if (!this.props.isReadyCurrent) {
      this.props.getCurrentArticle(id);
    }
  }

  moveOnList() {
    this.props.updateArticle();
    this.props.history.push("/auth/articles");
  }

  render() {
    return (
      <>
        <div>
          <button onClick={this.moveOnList}>Назад</button>
        </div>
        {this.props.isReadyCurrent ? <ArticleEditForm moveOnList={this.moveOnList}/> : ''}
      </>
    );
  }
}


let mapStateToProps = ({articles}) => {
  return {
    isReadyList: articles.isReadyList,
    isReadyCurrent: articles.isReadyCurrent,
    current: articles.current,
    nextId: articles.nextId
  }
};
let mapDispatchToProps = dispatch => ({
  getCurrentArticle: (id) => dispatch(getCurrentArticleId(id)),
  updateArticle: () => dispatch(updateArticle()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ArticleEdit);