import React, {useState} from 'react'
import style from './ArticleEditForm.module.css'
import {useFormik} from "formik";
import {connect} from "react-redux";
import CKEditor from '@ckeditor/ckeditor5-react';
import BalloonEditor from '@ckeditor/ckeditor5-build-balloon';
import {setUpdateArticle} from "../../../../../redux/articles-reducer";


const ArticleEditForm = ({moveOnList, current, setUpdateArticle}) => {
  const [ckvalue, setCkvalue] = useState(current.content);
  const formik = useFormik({
      enableReinitialize: true,
      initialValues: {
        id: current.id,
        title: !!current.title ? current.title : '',
        author: !!current.author ? current.author : 'Max Readman',
        date_published: !!current.date_published ? current.date_published : '',
      },
      onSubmit: values => {
        values['content'] = ckvalue;
        setUpdateArticle(values);
        moveOnList();
      }
    }
  );
  return <div>
    <h1>Статья</h1>
    <form onSubmit={formik.handleSubmit}>
      <label className={style['element-form']} htmlFor="id">ID</label>
      <input className={style['element-form']}
             id="id" disabled={true}
             name="id"
             placeholder="ID"
             onChange={formik.handleChange}
             value={formik.values.id}
      />

      <label className={style['element-form']} htmlFor="title">Заголовок</label>
      <input className={style['element-form']}
             id="title"
             name="title"
             placeholder="Заголовок"
             onChange={formik.handleChange}
             value={formik.values.title}
      />
      <label className={style['element-form']} htmlFor="author">Автор</label>
      <input
        className={style['element-form']}
        id="author"
        name="author"
        placeholder="Автор"
        onChange={formik.handleChange}
        value={formik.values.author}
      />
      <label className={style['element-form']} htmlFor="date_published">Дата публикации</label>
      <input
        className={style['element-form']}
        id="date_published"
        name="date_published"
        type="date"
        placeholder="Дата публикации"
        onChange={formik.handleChange}
        value={formik.values.date_published}
      />
      <CKEditor
        editor={BalloonEditor}
        config={
          {
            toolbar: ['undo', 'redo', '|', 'ckfinder', '|', 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote'],
            heading: {
              options: [
                {model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph'},
                {model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1'},
                {model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2'}
              ]
            },
            ckfinder: {
              openerMethod: 'modal',
            },
            image: {
              styles: [
                'alignLeft', 'alignCenter', 'alignRight'
              ],

              toolbar: [
                'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
                '|',
                'imageTextAlternative'
              ]
            }
          }
        }

        data={!!current.content ? current.content : ''}
        onChange={(event, editor) => {
          const data = editor.getData();
          setCkvalue(data);
        }}
      />
      <button className={style['element-form']} type="submit">Сохранить</button>
    </form>
  </div>;
};

let mapStateToProps = ({articles}) => {
  return {
    current: articles.current,
  }
};
let mapDispatchToProps = dispatch => ({
  setUpdateArticle: (data) => dispatch(setUpdateArticle(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ArticleEditForm);
