import React from 'react';
import {Redirect} from "react-router-dom";

const RedirectForAuth = () => {
  return <Redirect to={"/auth"}/>
};

export default RedirectForAuth;