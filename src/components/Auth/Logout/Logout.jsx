import React from 'react';
import {connect} from "react-redux";
import {setLogout} from "../../../redux/auth-reducer";
import {useHistory} from "react-router-dom";


function Logout({type, setLogout}) {
  let history = useHistory();

  const onMainPage = () => {
    history.push('/auth/articles');
  };

  return (
    <div>
      <h2>Действительно хотите выйти?</h2>
      <button onClick={onMainPage}>Отмена</button>
      <button onClick={setLogout}>Выйти</button>
    </div>
  );
}

let mapStateToProps = ({auth}) => {
  return {
    isAuth: auth.isAuth
  }
};

let mapDispatchToProps = (dispatch) => {
  return {
    setLogout: () => {
      dispatch(setLogout());
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Logout);