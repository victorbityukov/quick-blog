import React from 'react';
import {connect} from "react-redux";
import {checkMe} from "../../../redux/auth-reducer";
import {Redirect, Route, Switch} from "react-router-dom";
import SignIn from "../SignIn/SignIn";
import ArticleEdit from "../../Contents/Article/ArticleEdit/ArticleEdit";
import Logout from "../Logout/Logout";
import Articles from "../../Contents/Articles/Articles";
import ArticleDelete from "../../Contents/Article/ArticleDelete/ArticleDelete";


const Auth = ({isAuth, login, checkMe}) => {
  if (!isAuth) {
    checkMe();
  }
  return (
    <Switch>
      {isAuth ?
        <>
          {login !== '' ?
            <>
              <Route path="/auth/article/edit/:id" component={ArticleEdit}/>
              <Route path="/auth/article/delete/:id" component={ArticleDelete}/>
              <Route path="/auth/articles" component={Articles}/>
              <Route path="/auth/logout" component={Logout}/>
            </>
            : <Redirect to={"/auth/sign-in"}/>}
          {login === '' ?
            <Route path="/auth/sign-in" component={SignIn}/>
            : <Redirect to={"/auth/articles"}/>}
        </> : <Redirect to={"/auth/articles"}/>
      }
    </Switch>
  )
};

let mapStateToProps = ({auth}) => {
  return {
    login: auth.login,
    isAuth: auth.isAuth
  }
};

let mapDispatchToProps = (dispatch) => {
  return {
    checkMe: () => {
      dispatch(checkMe());
    }
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(Auth);