import React from 'react';
import {setLogin} from "../../../redux/auth-reducer";
import {useFormik} from "formik";
import {connect} from "react-redux";
import style from './SignIn.module.css';

const SignIn = ({setLogin}) => {
  const formik = useFormik({
    initialValues: {
      login: '',
      password: ''
    },
    onSubmit: (values, {setSubmitting}) => {
      let {login, password} = values;
      setLogin(login, password);
      setSubmitting(false);
    },
  });
  return (
    <div className={style['container']}>
      <h3>Sign in</h3>
      <form onSubmit={formik.handleSubmit}>
        <input
          required
          id="login"
          name="login"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.login}
          placeholder="Login"
        />
        <input
          name="password"
          type="password"
          id="password"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.password}
          placeholder="Password"
        />
        <button
          type="submit"
        >
          Sign In
        </button>
      </form>
    </div>
  )
};


let mapStateToProps = () => {
  return {}
};

let mapDispatchToProps = (dispatch) => {
  return {
    setLogin: (login, password) => {
      dispatch(setLogin(login, password));
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);